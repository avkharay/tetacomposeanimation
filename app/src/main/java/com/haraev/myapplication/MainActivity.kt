package com.haraev.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.DrawableRes
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.haraev.myapplication.ui.theme.MyApplicationTheme
import kotlin.math.roundToInt

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    color = MaterialTheme.colors.background
                ) {
                    Greeting()
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Greeting() {
    val state = rememberSwipeableState(initialValue = SwipingState.START)
    Box(
        Modifier
            .fillMaxWidth()
            .height(56.dp)
            .background(Color.Green, shape = RoundedCornerShape(20.dp))) {
        val endAnchor = LocalConfiguration.current.densityDpi / 160f * (LocalConfiguration.current.screenWidthDp - 56).toFloat()

        AnimatedVisibility(
            visible = state.currentValue == SwipingState.START,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(text = "Order 1000 rub")
                Text(text = "Swipe to confirm")
            }
        }

        Image(
            painter = painterResource(id = state.currentValue.icon),
            contentDescription = null,
            modifier = Modifier
                .offset { IntOffset(state.offset.value.roundToInt(), 0) }
                .size(56.dp)
                .swipeable(
                    state = state,
                    anchors = mapOf(0f to SwipingState.START, endAnchor to SwipingState.END),
                    thresholds = { _, _ -> FractionalThreshold(1f) },
                    orientation = Orientation.Horizontal
                )
                .background(Color.White, CircleShape),
            colorFilter = ColorFilter.tint(Color.Green)
        )
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyApplicationTheme {
        Greeting()
    }
}

enum class SwipingState(@DrawableRes val icon: Int) {
    START(R.drawable.ic_arrow_right),
    END(R.drawable.ic_check)
}